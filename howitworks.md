# Как найти ваш репозиторий?

![Screen Shot 2017-12-21 at 4.01.36 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/266719937-Screen%20Shot%202017-12-21%20at%204.01.36%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.01.47 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/2578269014-Screen%20Shot%202017-12-21%20at%204.01.47%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.02.09 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/249005287-Screen%20Shot%202017-12-21%20at%204.02.09%20PM.jpeg)




# Что где лежит в репозитории и куда нажимать при разных ситуациях?

![Screen Shot 2017-12-21 at 4.02.48 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/260981361-Screen%20Shot%202017-12-21%20at%204.02.48%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.05.15 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/3364716943-Screen%20Shot%202017-12-21%20at%204.05.15%20PM.jpeg)




# Где посмотреть историю изменений файла?

![Screen Shot 2017-12-21 at 4.03.55 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/1371187632-Screen%20Shot%202017-12-21%20at%204.03.55%20PM.jpeg)



# Как вносить изменения и редактировать файл?

![Screen Shot 2017-12-21 at 4.06.04 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/1186490095-Screen%20Shot%202017-12-21%20at%204.06.04%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.06.15 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/581553996-Screen%20Shot%202017-12-21%20at%204.06.15%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.06.27 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/2588059508-Screen%20Shot%202017-12-21%20at%204.06.27%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.06.59 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/3429161570-Screen%20Shot%202017-12-21%20at%204.06.59%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.07.08 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/3694044001-Screen%20Shot%202017-12-21%20at%204.07.08%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.07.36 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/1015987575-Screen%20Shot%202017-12-21%20at%204.07.36%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.08.36 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/2264804607-Screen%20Shot%202017-12-21%20at%204.08.36%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.08.49 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/1027252438-Screen%20Shot%202017-12-21%20at%204.08.49%20PM.jpeg)




# Как посмотреть, что получилось в нормальном режиме?

![Screen Shot 2017-12-21 at 4.46.33 PM.jpg](https://bitbucket.org/repo/oLg9qXr/images/1720046110-Screen%20Shot%202017-12-21%20at%204.46.33%20PM.jpg)




# Как написать редактору если вы сомеваетесь по какому-то фрагменту?

![Screen Shot 2017-12-21 at 4.07.59 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/38149366-Screen%20Shot%202017-12-21%20at%204.07.59%20PM.jpeg)


или

![Screen Shot 2017-12-21 at 4.09.26 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/3624938204-Screen%20Shot%202017-12-21%20at%204.09.26%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.10.10 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/2454800110-Screen%20Shot%202017-12-21%20at%204.10.10%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.10.48 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/3258402660-Screen%20Shot%202017-12-21%20at%204.10.48%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.11.05 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/452223916-Screen%20Shot%202017-12-21%20at%204.11.05%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.11.46 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/87986618-Screen%20Shot%202017-12-21%20at%204.11.46%20PM.jpeg)

![Screen Shot 2017-12-21 at 4.11.57 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/1085840263-Screen%20Shot%202017-12-21%20at%204.11.57%20PM.jpeg)




# Как пометить для редактора, что изменение в файлы вы считаете принятым?

![Screen Shot 2017-12-21 at 4.12.28 PM.jpeg](https://bitbucket.org/repo/oLg9qXr/images/1508697624-Screen%20Shot%202017-12-21%20at%204.12.28%20PM.jpeg)


