# Serendipity Lab Inc. Independent Contractor Agreement

## 1. PARTIES:

This Independent Contractor Agreement is entered into between I Serendipity Lab Inc. (“Company”), Delaware corporation with its registered office located at 919 North Market Street, Suite 950 in the City of Wilmington, County of New Castle, Zip 19801, and

.........................................., ("you"),

and shall be effective from  ..................... (“Effective Date”).

## 2. SERVICES

on Exhibit A

## 3. PAYMENT

on Exhibit B 

## 4. ADDITIONAL TERMS

The following "Key Person" shall be primarily responsible for completing the services:

  ..................... 
  
BY SIGNING BELOW, YOU AGREE TO ALL THE TERMS AND CONDITIONS CONTAINED ON BOTH SIDES OF THIS PAGE AND ANY ATTACHED SCHEDULES, ALL OF WHICH TOGETHER CONSTITUTE THE FULL AND FINAL AGREEMENT BETWEEN YOU AND THE COMPANY.  THIS AGREEMENT MAY ONLY BE MODIFIED BY A WRITTEN AGREEMENT SIGNED BY THE AUTHORIZED REPRESENTATIVES OF YOU AND THE COMPANY.


INDEPENDENT CONTRACTOR TERMS AND CONDITIONS

## 1. Independent Contractor Relationship

a. Company has engaged your services as an independent contractor because you have represented that your skills are unique or higher than those of Company's regular employees.  Accordingly, you will not receive any instruction or training from Company and you will have the exclusive right to control and direct the details of how you perform the services. You are free to set your own work hours, but you must meet the deadlines or other requirements set forth on the face of this Agreement.  You must provide your own equipment and tools and provide the services at your location.  In some circumstances, however, Company may allow you to use Company's office or equipment on an incidental and voluntary basis.  You are not entitled to any benefits that are provided by Company to its employees, including but not limited to stock, stock options, health, insurance or other benefits.

b. Nothing contained in this Agreement shall be construed to create an employee-employer relationship or a partnership or joint venture between you and Company.  You have no authority to act on behalf of Company, or to enter into any contract, or to incur any liability on behalf of Company.

## 2. Payment. 

All payments shall be made net 45 days from the date Company receives an acceptable invoice from you.  You MUST invoice, and, if appropriate, supply supporting documentation in order to be paid.  Your invoices and correspondence shall refer to the purchase order number and the project number, if one has been assigned.  Your invoices must specify the results achieved, not just methods or hours of work. You must meet any schedule and/or deadlines set forth on the face of this Agreement for your invoices to be honored. 

## 3. Taxes.

a. Prior to commencing the services, you must provide Company with your IRS Form W-9, Employer Identification Number or Tax ID #.

b. You are responsible for filing all tax returns, tax declarations, and tax schedules, and for the payment of all taxes resulting from your fees or other compensation.  Company will not withhold any employment taxes from your fees.  Company will report the amount it pays you on IRS Form 1099, to the extent required to do so by law.

## 4. Insurance.

During the term of this Agreement, you shall maintain at your expense, for yourself and your employees, workers’ compensation, automobile collision and liability insurance, errors and omissions insurance, and other insurance as appropriate or required by law, including, but not limited to, comprehensive general liability insurance in the amount of $1,000,000 per occurrence.

## 5. Key Persons.  

If a person is designated as a "Key Person" on the face of this Agreement, it is because Company has determined that such person is uniquely talented or qualified to perform the services.  Company has negotiated for and is paying a premium price for the services of the Key Person.  Accordingly, you will ensure that the Key Person shall be primarily responsible for performing the services.

## 6. Subcontractors. 

You have the right to engage your own employees and subcontractors to assist you in performing the services. You assume full responsibility for the actions of your employees and subcontractors and you shall be solely responsible for the payment of their salary (including withholding and/or remitting income taxes, social security or other taxes or fines) and for providing worker’s compensation, disability and other benefits.  You shall indemnify and hold Company harmless against any and all liabilities for your failure to do so. 

## 7. Other Customers. 

You may perform services for other customers, so long as the performance of these services does not interfere with your performance under this Agreement.

## 8. Termination. 

Company may terminate your services at any time immediately upon written notice. In such event Company will pay you a pro-rated fee and will own all work done to that point.

## 9. Confidentiality

a. “Confidential Information” means all information relating to the business or assets of Company that is not generally known by non-Company personnel, including information you create or discover while performing the services under this Agreement, and information obtained by Company in confidence from third parties.  The existence and terms of this Agreement shall be Confidential Information.

b. You agree that you will not disclose, use or copy the Confidential Information, except as required in the lawful performance of your duties to Company.  You will take all reasonable measures to protect the Confidential Information from any unauthorized or premature disclosure, use or destruction.  Your obligation to protect the Confidential Information shall survive at all times after the termination of your engagement with Company.

c. Although you are not restricted from working with a person or entity which has independently developed information or materials similar to the Confidential Information, you agree not to disclose the fact that any similarity exists, and such similarity does not excuse you from your obligations to Company.

## 10. Ownership rights

a. You hereby assign to Company all right, title and interest in and to the work performed by you under this Agreement (including all copyrights, trademarks, trade secret rights patent rights and other proprietary rights with respect to the work).  You agree not to use the work created under this Agreement for your own benefit or the benefit of any party other than Company.  You agree to execute any other documents as Company may reasonably require from time to time to register, record or perfect the rights granted hereunder.

b. You hereby irrevocably appoint the President and General Counsel of Company each to act as your agent and attorney-in-fact to perform all acts necessary to obtain and/or maintain patents, copyrights, mask work rights, and similar rights, to any creations assigned to Company by you under this Agreement if (a) you are unavailable, within the meaning of any applicable laws, or (b) you refuse to perform those acts.  You acknowledge that the grant of the foregoing power of attorney is coupled with an interest and shall survive the death, disability, liquidation and/or dissolution of you or any of your employees.

## 11. Prior Rights and Obligations

a. You warrant that you have the right to disclose and use any information used by you in the performance of the services without breaching any agreement you have with any other party.

b. If there is any conflict between your obligations to Company and (a) any obligations you have to protect the confidentiality of any other party's information or materials, or (b) any rights you claim to any inventions or ideas of your own, you will inform Company of the conflict prior to performing the work.  Unless notified, Company will conclude that no such conflict exists, and you agree thereafter to hold Company harmless against any claims arising from any such conflict.  Company shall receive all such disclosures in confidence.

## 12. LIMITATION OF LIABILITY.  

IN NO EVENT SHALL Company BE LIABLE TO YOU FOR ANY CONSEQUENTIAL, INDIRECT, SPECIAL OR INCIDENTAL DAMAGES ARISING FROM OR RELATING TO THIS AGREEMENT, EVEN IF Company HAS ABEEN ADVISED OF THE POSSIBILITY OF SUCH POTENTIAL LOSS OR DAMAGE.  IN NO EVENT SHALL Company BE LIABLE FOR AMOUNTS IN EXCESS OF THE AMOUNTS PAYABLE IN ACCORDANCE WITH THE TERMS OF THIS AGREEMENT.

## 13. General

a. You may not assign this Agreement or any of your rights or duties hereunder to any other party without Company’s prior written consent.  Company may not assign this Agreement to any other party without prior notice to you.

b. You agree to hold harmless and indemnify Company for all expenses or liability arising from any injury, damages, or liability incurred in connection with your performance under this Agreement, except to the extent that it is caused by Company’s own negligence.

c. Company shall not be deemed to be in breach of any of its obligations hereunder unless given written notice of the breach by certified or registered mail, return receipt requested, specifying the nature of the breach and Company shall have thirty days from the date of the notice to cure the breach.

d. Neither party will solicit for employment the employees of the other party for a period of ninety (90) days following the completion of efforts under this Agreement, however, general advertisements, job postings, use of third party recruiters (not specifically directed to the employees of the other party), and responding to inquiries regarding employment initiated by employees of the other party shall not be deemed a solicitation in violation of this provision.

e. This Agreement shall be governed by and construed under the laws of the State of California excluding its conflict of laws rules.  Any claim or controversy relating to this Agreement shall be fully and finally resolved by binding arbitration administered by the American Arbitration Association under its Commercial Arbitration Rules.  The arbitration shall take place in Santa Clara County, California, and judgment on the award may be entered in any court having jurisdiction thereof.


# Exhibits for editing


# Exhibit A: SERVICES

1. Text transcribing from video/ audio records.
2. Text editing on the base of common process guidance.



# Exhibits for system administrator

Andrey iAdviser, [Mar 10, 2018, 12:30:31 PM]:
Полное администрирование сервера и мониторинг (до 3x VPS, до 4ч/мес):

1. Мониторинг доступности и загруженности сервера, работоспособности отдельных сервисов, дисковых массивов.
2. Еженедельное обновление программного обеспечения и самой ОС на сервере.
3. Настройка и оптимизация работы MySQL/Nginx/Apache и др. индивидуально под каждый проект.
4. Полный ежедневный бекап на удаленный сервер данных проектов клиента и восстановление данных из бэкапа по требованию клиента.
5. Разделение доступа к различным данным для разных пользователей.
6. Установка и настройка дополнительного программного обеспечения по запросу.
7. Создание и настройка почтовых аккаунтов, FTP/SSH и т.д.
8. Базовая помощь в защите от DDoS-атак средствами самого сервера
9. Советы и консультации по оптимизации.

Full server administration and monitoring (up to 3x VPS, up to 4h / month):

1. Monitoring the availability and load of the server, the health of individual services, disk arrays.
2. Weekly update of the software and the OS itself on the server.
3. Configure and optimize MySQL / Nginx / Apache, etc. individually for each project.
4. Full daily backup to the remote data server of the client's projects and data recovery from the backup on client's request.
5. Separation of access to different data for different users.
6. Installation and configuration of additional software on request.
7. Create and configure mail accounts, FTP / SSH, etc.
8. Basic help in protecting against DDoS attacks by the server itself
9. Tips and advice on optimization.
