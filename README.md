# Процесс разработки курса

![Screen Shot 2018-01-24 at 8.56.58 PM.png](https://bitbucket.org/repo/oLg9qXr/images/2238120787-Screen%20Shot%202018-01-24%20at%208.56.58%20PM.png)

# Открытые вакансии

1. **Редактор** - типовая инструкция с примерами редактирования находится в файле Instructions-Editor.md
2. **Разработчиков курсов** - описание вакансии и инструкция находится в файле Instructionl-designer.md
3. Специфические детали, применимые для конкретного задания изложены в репозиториях соответствующих заданий.

# Как попасть к нам в проект?

1. Прочитать настоящий документ, понять, что мы делаем и как работаем.
2. Прочитать описание вакансий, чтобы понять какие требования мы предъявляем и в чем состоит работа?
2. Если все понятно, то заполнить [анкету](https://alexeykrol.typeform.com/to/ZjjT3j)
3. Если по результатам рассмотрения анкеты Вы нам подходите, то мы напишем и предложим подписать необходимые документы (см. нижн).
4. Вы подписываете документы.
5. Мы направляем инвайт в ваш рабочий репозиторий, где уже лежат файлы, с которыми вы будете работать и более точные инструкции. Разработчикам курса также будет прислан инвайт в систему управления проектами и на тестовый сайт.
6. Вы начинаете работать, в рабочем порядке общаться со своим менеджером и другими членами команды, радоваться и все такое.
7. Когда работа принята, вы получаете деньги.
8. Мы даем новые задания и так до победы!
9. Если вы очень круты, способны обучаться и есть амбиции, то мы предлагаем более серьезную и интересную работу. Если Вы не заинтересованы и работаете не очень, мы расстаемся.


#### Все люди, сотрудничающие или работающие в Редакции, работают на основе 3-х документов:

1. **Типовой контракт**. Его текст на английском языке можно найти в документе под названием **contract.md**, который находится в этой же папке.
2. **Типовое соглашение о неразглашении** - NDA. Его текст на английском языке можно найти в документе под названием **NDA.md**, который находится в этой же папке. Мы впервые ввели это требование после того, как обнаружили, что несколько раз копирайтеры, которых мы нанимали, воровали наши тексты, изменяли и размещали на своих ресурсах или продавали другим заказчикам. Подписание NDA даёт возможность через внесудебные и судебные процедуры предъявить претенции любому, вплоть до остановки и блокировки сайтов.
3. **Подтверждение места жительства.** Этот документ мы обязаны предоставлять в налоговые органы США, чтобы избежать двойного налогообложения и штрафов. Этот документ не налагает никаких обязательств, по сути никем не проверяется, и в нем надо указать, что вы живете в такой-то стране по такому то адресу. Ваш адрес тоже по сути никто проверять не будет, важно указать страну, которая входит в соглашение об избежании двойного налогообложения. Россия/ Украина/ Беларусь/ страны СНГ входят в это соглашение. Это стандартная форма, которую мы не можем менять. Скачать и посмотреть эту форму можно на сайте [налоговой службы США](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf)


#### Подписание данных документов.

1. Типовое соглашение о неразглашении является офертой, что означает, что как только вы получили доступ к документу, вы автоматически принимаете на себя обязательства, возникающие и описанные в соглашении.
2. Типовой контракт и Подтверждение места жительства вы подписываете в электронном виде. Для этого вам будет выслана ссылка и инструкция как это делать. Там ничего сложного нет - надо просто несколько раз кликнуть мышкой, все ваши данные будут введены заранее.
3. Инструкции  является офертой, что означает, что как только вы получили доступ к документу, вы автоматически принимаете на себя обязательства, возникающие в Инструкции.

#### Подписание электронных документов

1. Вы подписываете в электронной форме Типовой контракт и Подтверждение места жительства.
2. Для этого вам будет выслана ссылка и инструкция как это делать. Там ничего сложного нет - надо просто несколько раз кликнуть мышкой, все ваши данные будут введены заранее.
3. Для подписания мы используем сервис DocuSign - https://en.wikipedia.org/wiki/DocuSign - Вы можете заранее ознакомиться с принципами его работы.
4. Перед подписанием, рекомендуем посмотреть [короткое видео как подписывать электронные документы](https://vimeo.com/248529063).

Перед началом любой работы все сотрудники подписывают эти документы. Отказ подписать означает невозможность сотрудничать. Учитывайте тот факт, что подписаные договора защищают и нас и вас на территории США (что более надежно, чем иные юрисдикции).

#### Уведомления о подписании документов

1. Единственный орган, который уведомляется о наших правоотношениеях и о том, что мы платим вам за работу, это налоговая служба США.
2. Никаких уведомлений в налоговые органы ваших стран мы не посылаем.


# Мы работаем с системой контроля версий BitBucket

1. Это означает, если вы хотите с нами работать, то вы либо должны уметь работать с подобными системами, типа BitBucket или Github, либо освоить в процессе, на что у вас уйдет минут 10-20. Пугаться не надо. Конечно, сначала будет непривычно, но вы быстро освоитесь, тем более, что до вас уже этому научились десятки миллионов людей. Это проще,чем писать посты в Instagram, хотя сначала кажется, что сложно.

2. Система контроля версий позволяет каждому редактору работать в собственном репозитории (папке), контролировать любое измнение, т.е. никто не сможет сказать, что вы чего-то не сделали, позволяет совместно работать на текстом, не мешая друг другу, а также, что важно, отслеживать авторство измнений и сами изменения. Если вы или кто-то случайно внёс изменения в ваш текст, то это всё можно легко восстановить, и ничего не может потеряться.

3. Также система контроля версий защищает от воровства, так как фиксирует момент, когда редактор получает доступ к документу. Это означает, что всегда можно доказать авторство и первенство.

4. Вы можете работать с текстом как через веб браузер, так и через приложение для компьютера. Надо немного потратить времени, чтобы его скачать, установить и настроить, но за то вы можете работать с текстами локально, а система автоматически обновляет ваши измнения на сервере.

5. Мы рекомендуем работать через браузер, потому что это очень легко, но если кто особо упёртый, и хочет установить приложение, то напишите и мы дадим ссылку на то, что рекомендуем.

#### Почему имеет смысл научиться работать с системой контроля версий?

1. Тенденция такая, что все больше компаний, работающих с контентом, начинают использовать системы контроля версий по описанным выше причинам, поэтому если вы вставляете этот навык в ваше резюме, это повышает ваши шансы на рынке труда.
2. А если вы сами работаете над оригинальным контентом, и сами хотите нанять внешних редакторов или копирайтеров, то вот тут вы поймете, что это самый лучший способ защищать свой труд и эффективно управлять процессом.




# Если Вы хотите работать с нами, идите по шагам:

1. Заполнить [анкету](https://alexeykrol.typeform.com/to/ZjjT3j). Если Вы нам подходите, то мы свяжемся с Вами.
2. Подписать документы.
3. Получить приглашение в репозиторий.
4. Ознакомиться и задать вопросы.

Простая инструкция как работать в системе - в файле howitworks.md


# Что такое редакция, как она организована и как работает?

#### Общие положения

1. Редакция это коллектив людей, которые живут в разных точках планеты, но работают вместе, чтобы создавать отличный и полезный контент.
2. Результатом нашей работы являются тексты или инфографика статей, книг, курсов, семинаров.
3. Входом нашей работы являются сырые тексты, расшифровки, аудио или видео касты, рисунки.
4. Руководит редакцией **Татьяна Крол**.
5. В Редакции работают редакторы, расшифровщики, копирайтеры и рерайтеры, дизайнеры, менеджеры.


#### Как общаться друг с другом?

1. У каждого в Редакции есть ник, это имя, которое вы присвоили при регистрации. К примеру, ник руководителя редакции Татьяны в системе **@Otanna** - вы всегда можете написать ей, когда даёте комменты. 
2. Также у нас есть общий канал в [Slack](https://alexkrol.slack.com/messages/G6QE4JAB0), где можно общаться по разным вопросам в оперативном режиме. Все, кто имеет доступ туда - знают как это делать. Канал Slack, как правило, для менеджеров и старших редакторов, с теми, с кем мы работаем на регулярной основе.
3. Также у нас есть общий канал в Telegram для внешних редакторов, расшифровщиков, дизайнеров, художников, т.е. тех, кого мы привлекаем и с кем обсуждаем общие вопросы. Если вы приглашены в Редакцию, то вы автоматически получите приглашение в канал. Ссылка на кагал в Telegram будет в вашем персональном репозитории.

#### Как организована редакция?

1. Редакция работает над - **Проектами**. Если представить, что сама **Редакция** - это здание, то в данном случае **Проекты** это этаж, а на каждом этаже есть комнаты - **Репозитории**, а внутри комнаты уже есть шкафы - это папки, в которых лежат файлы, с которыми мы работаем.
2. Таким образом у каждого редактора или сотрудника может быть доступ ко всем проектам (**Проекты**), к какому-то специфическому, а также доступ ко всем комнатам (**Репозиториям**) или какому-то специфическому. Это сделано для того, чтобы не мешать друг другу. Это позволяет ограничить доступ к какому-то одному **Репозиторию** только для тех, кто работает с текстами данного **Репозитория**.
3. Главный редактор имеет доступ ко всем **Проектам** и ко всем **Репозиториям**. Остальные сотрудники имеют доступ к тем **Проектам** и **Репозиториям**, которые нужны для их работы. Кто-то может иметь доступ к нескольким **Проектам** и **Репозиториям**, а кто-то только к конкретному **Repository**.

#### Проекты (этажи) редакции (Projects)

1. [Курсы и уроки](https://bitbucket.org/account/user/serendipity_1/projects/COURSE)		Все **закрытые** проекты, над которыми работает редакция. Сюда имеют доступ только те, кому он предоставлен.
2. [Предбанник](https://bitbucket.org/account/user/serendipity_1/projects/EDITOR) - Все **открытые проекты**, над которыми работает редакция - сюда имеют доступ все.
3. [Книга об ICO](https://bitbucket.org/serendipity_1/kniga-ob-ico) - соответственно книга об ICO, **закрытый** репозиторий
4. [Теория Каст и Ролей 2](https://bitbucket.org/serendipity_1/teoriia-kast-i-rolei) - работа над проектом Теории Каст и Ролей 2, **закрытый** репозиторий


#### Репозитории (комнаты) редакции (Repository)

1. [Инструкции для новичков](https://bitbucket.org/serendipity_1/novichkam/src) - данный репозиторий, в котором информация для новичков.
2. [Общий](https://bitbucket.org/serendipity_1/obshchii) - общий **закрытый** репозиторий проекта "Книг об ICO".
3. [Общие расшифровки](https://bitbucket.org/serendipity_1/obshchie-rasshifrovki) -   **закрытый** репозиторий для всех расшифровок.


#### Оплата и условия

Условия для разных позиций в файле conditions.md
